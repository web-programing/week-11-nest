import { IsInt, IsNotEmpty, Length, Max, Min } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(1, 50)
  name: string;

  @IsInt()
  @Min(0)
  @Max(1000)
  @IsNotEmpty()
  price: number;
}
