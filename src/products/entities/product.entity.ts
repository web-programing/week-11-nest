import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'product_name' }) //name: 'product_name' คือการกำหนดชื่อ attribute ใน database
  name: string;

  @Column({ name: 'product_price' })
  price: number;
}
